# detached-sigs


This a repository to store nexa detached signatures for nexa macOS (arm64 and x86) applications.

For every new lease a new branch will be created where the signatures will be stored, e.g. there is a branch called 1.4.0.1, then one 2.0.0.0 etc.
